package net.jc.sample.openshift.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class Product {


        @Id
        @GeneratedValue
        private int id;

        private String nom;
        private int prix;
        private int prixAchat;

}
